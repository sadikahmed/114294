<?php
//EQUAL CONDITIONAL OPERATORS
$a="150";
$b="100";
$br= "<br>";

//SAME CONDITIONAL OPERATORS

if($a==$b)
{
    print "same$br";
}
else
{
    print "not same$br";
}

//SAME VALUE AND TYPE CONDITIONAL OPERATORS
if($a===$b)
{
    print "same value and type$br";
}
else
{
    print "not same value and type$br";
}
//NOT EQUAL
if($a!=$b)
{
    print "Not Equal$br";
}
else
{
    print "Equal$br";
}
//NOT EQUAL ANOTHER
if($a<>$b)
{
    print "Not Equal$br";
}
else
{
    print "Equal$br";
}
//NOT SAME
if($a!==$b)
{
    print "Not Same$br";
}
else
{
    print "Same$br";
}
//GETTER THAN
if($a>$b)
{
    print "$a is Getter Than $b$br";
}
else
{
    print "$b is smaller than $a$br";
}